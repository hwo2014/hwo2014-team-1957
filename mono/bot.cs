using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

public class Bot
{
    class TrackPiece
	{
		public double length,radius, angle;
		public bool hasSwitch;
		public bool isCorner;
	}

	List<TrackPiece> track = new List<TrackPiece>();

	int currentPieceNumber = 0;
	int totalTrackPieceAmount = 0;

	//double maxStraightSpeed = 1;
	//double maxCornerSpeed = 0.7;

	int[] lanes = new int[10];
	int lanesTotal = 0;

	int currentTick = 0;
	//double currentSpeed = 0;
	double inPieceDistance = 0;

	double currentAngle = 0;

	double nextThrottle = 0;
	double maxAngle = 55;
	int securityAngleThreshold = 15;

   // bool switched = false;

	public static void Main(string[] args)
    {
        string host = args [0];
        int port = int.Parse(args [1]);
        string botName = args [2];
        string botKey = args [3];

        Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        using (TcpClient client = new TcpClient(host, port))
        {
            NetworkStream stream = client.GetStream();
            StreamReader reader = new StreamReader(stream);
            StreamWriter writer = new StreamWriter(stream);
            writer.AutoFlush = true;

            new Bot(reader, writer, new Join(botName, botKey));
        }
    }

    private StreamWriter writer;

    Bot(StreamReader reader, StreamWriter writer, Join join)
    {
        this.writer = writer;
        string line;

        send(join);

        while ((line = reader.ReadLine()) != null)
        {
            MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
			//Console.WriteLine(msg.msgType);

			switch (msg.msgType)
            {
                case "carPositions":
					//Console.WriteLine("Car positions");
					//Console.WriteLine(msg.data);
					currentTick++;
					ParseCarPosition(msg.data as JArray);
					//send(new Throttle(GetMaxSpeedForPiece()));	

//                    if(currentPieceNumber == 3)
//                    {
//                        if (!switched)
//                        {
//                            send(new SwitchLane(SwitchLane.Direction.Right));
//                            switched = true;
//                        }
//                        else
//                            send(new Throttle(0));
//
//                    } 
//                    else
//    //                    send(new Throttle(0.2));

					send(new Throttle(nextThrottle));
                    break;
                case "join":
                    Console.WriteLine("Joined");
					Console.WriteLine(msg.data);
                    send(new Ping());
                    break;
				case "yourCar":
					Console.WriteLine("Your car");
					//Console.WriteLine(msg.data);
					break;
				case "gameInit":
                    Console.WriteLine("Race init");
					//Console.WriteLine(msg.data);
					
					ParseGameInit(msg.data as JObject);

					
                    send(new Ping());
                    break;
                case "gameEnd":
                    Console.WriteLine("Race ended");
					//Console.WriteLine(msg.data);	
                    send(new Ping());
                    break;
                case "gameStart":
                    Console.WriteLine("Race starts");
					//Console.WriteLine(msg.data);
                    send(new Ping());
                    break;
				case "crash":
					Console.WriteLine("*****Crash******" + currentAngle);
//					if (currentAngle < maxAngle)	
//						maxAngle = currentAngle;
//					else
//						securityAngleThreshold+=5;
					break;
                default:
					Console.WriteLine(msg.msgType.ToString());
					Console.WriteLine(msg.data);
                    send(new Ping());
                    break;
            }
        }
    }

	private void ParseGameInit(JObject gameInit)
	{
		//string trackId = (string)gameInit["race"]["track"]["id"];
		//string trackName = (string)gameInit["race"]["track"]["name"];

		lanesTotal = 0;
		foreach (var lane in gameInit["race"]["track"]["lanes"].Children())
		{
			if (lane["distanceFromCenter"] != null)
			{
				lanes[lanesTotal] = (int)lane["distanceFromCenter"];
				lanesTotal++;
			}

		}


		int pieceAmount = 0;
		foreach (var piece in gameInit["race"]["track"]["pieces"].Children())
		{
			TrackPiece newPiece = new TrackPiece();

			if (piece["length"] != null)
			{
				newPiece.length = (double)piece["length"];
			}
			if (piece["radius"] != null)
			{
				newPiece.radius = (double)piece["radius"];
				newPiece.isCorner = true;
			}
			if (piece["angle"] != null)
			{
				newPiece.angle = (double)piece["angle"];
				newPiece.isCorner = true;
			}
			if (piece["switch"] != null)
			{
				newPiece.hasSwitch = (bool)piece["switch"];
			}
			if (newPiece.isCorner)
				newPiece.length = GetCornerLength(newPiece.radius,newPiece.angle,lanes[0]); //TODO CHANGE!!!
			track.Add(newPiece);
			pieceAmount++;

		}
		totalTrackPieceAmount = pieceAmount;
	}

	double GetCornerLength(double radius, double angle, int laneOffset)
	{
		return (2 * Math.PI * (radius - laneOffset) * (Math.Abs(angle) / 360.0));

	}

	private void ParseCarPosition(JArray carPositions)
	{
		double prevPieceDistance = inPieceDistance;
		int prevPieceNumber = currentPieceNumber;
		int nextPieceNumber = (currentPieceNumber + 1)  % totalTrackPieceAmount;
		 
		foreach (var carPosition in carPositions)
			if((string)carPosition["id"]["name"] == "Who cares")
			{
				if (carPosition["piecePosition"]["pieceIndex"] != null)
					currentPieceNumber = (int)carPosition["piecePosition"]["pieceIndex"];
				if (carPosition["piecePosition"]["inPieceDistance"] != null)
					inPieceDistance = (int)carPosition["piecePosition"]["inPieceDistance"];
				if (carPosition["angle"] != null)
					currentAngle = (int)carPosition["angle"];
			}

		double vDistance = 0;

		if (prevPieceNumber == currentPieceNumber)
			vDistance = inPieceDistance - prevPieceDistance;
		else 
			vDistance = inPieceDistance + track[prevPieceNumber].length - prevPieceDistance;


		//double prevSpeed = currentSpeed;
        if (track[currentPieceNumber].isCorner)
        {
            if (Math.Abs(currentAngle) < maxAngle - securityAngleThreshold && vDistance < 7)
            {
                    nextThrottle = 0.7;
            }
                
            else
                nextThrottle = 0;
        }
        else
        {
            if (track[nextPieceNumber].isCorner && vDistance > 7)
                nextThrottle = 0;
            else
                nextThrottle = 0.9;
        }	

		//Console.WriteLine(prevPieceNumber.ToString() + " " + prevPieceDistance + " " + currentPieceNumber + " " + inPieceDistance + " " + vDistance);
       // Console.WriteLine(currentPieceNumber + " " +  track[currentPieceNumber].isCorner + " "  + inPieceDistance + " " + vDistance + " " + currentAngle);	
	}

	double GetMaxSpeedForPiece()
	{
		int nextPieceNumber = (currentPieceNumber + 1)  % totalTrackPieceAmount;
		int prevPieceNumber = (currentPieceNumber - 1)  % totalTrackPieceAmount;

		if (track[currentPieceNumber].isCorner)
		{
			if (track[currentPieceNumber].angle > 40)
				return 0.655;
			else
				return 0.7;
		}
		else
		{
			if (track[nextPieceNumber].isCorner)
			{
				if(track[prevPieceNumber].isCorner)
			    {
					return 0.7;
				}
				else
				{
					return 0.3;
				}   
		   	}
	   		else
			   return 0.9;
		}
	}

	double GetMaxCorneSpeed()
	{
		return 0;
	}

    private void send(SendMsg msg)
    {
        writer.WriteLine(msg.ToJson());
    }

}

class MsgWrapper
{
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data)
    {
        this.msgType = msgType;
        this.data = data;
    }
}


abstract class SendMsg
{
    public string ToJson()
    {
        return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
    }

    protected virtual Object MsgData()
    {
        return this;
    }

    protected abstract string MsgType();
}

class SwitchLane : SendMsg
{
    public enum Direction
    {
        Left,
        Right,
        Nope
    }
    
    public Direction value;
    
    public SwitchLane(Direction value)
    {
        if (value == Direction.Nope)
            throw new System.Exception("This value is not for sending");
        this.value = value;
    }
    
    protected override Object MsgData()
    {
        return this.value.ToString();
    }
    
    protected override string MsgType()
    {
        return "switchLane";
    }
}

class Join: SendMsg
{

    public string name;
    public string key;

    public Join(string name, string key)
    {

        this.name = name;
        this.key = key;
    }

    protected override string MsgType()
    { 
        return "join";
    }
}


class Ping: SendMsg
{
    protected override string MsgType()
    {
        return "ping";
    }
}


class Throttle: SendMsg
{
    public double value;

    public Throttle(double value)
    {
        this.value = value;
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "throttle";
    }
}